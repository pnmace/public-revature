

window.onload = function() {

    document.getElementById("sendLogin").addEventListener('click', login);

}

function login(event) {
    event.preventDefault();

    let loginUsername = document.getElementById("username").value;
    let loginPassword = document.getElementById("password").value;

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let response = xhttp.responseText;
            console.log(response);
            if (response == "invalid\n") {
                invalidlogin();
            }
        }
    }

    // this is subject to change! I am no longer using /tickets
    xhttp.open('POST', 'http://localhost:9001/Project1/forwarding/login');

    xhttp.setRequestHeader("Content-Type", "application/json");

    let loginData = {
        username: loginUsername,
        password: loginPassword
    };

    xhttp.send(JSON.stringify(loginData));
}

function invalidlogin() {
    var container = document.createElement("span");
    let divtext = document.createTextNode("Invalid login credentials");
    let div = document.querySelector('.form-group:last-child');

    container.appendChild(divtext);
    container.style.color = "red";

    div.appendChild(container);
}