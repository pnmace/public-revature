document.getElementById("logoutButton").addEventListener('click', logout);

window.onload = function() {

    ajaxGetUser();

    ajaxGetAllTickets();

}

async function logout() {
    let logoutResponse = await fetch('/Project1/user/logout');
    // wait for the logout to finish before redirecting
    let ack = await logoutResponse.json();

    window.location.replace("http://localhost:9001/Project1");
}

function ajaxGetUser() {

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (xhttp.readyState==4 && xhttp.status==200) {1
            let currentUser = JSON.parse(xhttp.responseText);

            userInformation(currentUser);
        }
    }

    xhttp.open("GET", `http://localhost:9001/Project1/user/json/getCurrentUser`)
    xhttp.send();


}

function ajaxGetAllTickets() {
    
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {

        if (xhttp.readyState==4 && xhttp.status==200) {
            let tickets = JSON.parse(xhttp.responseText);
            console.log(tickets);

            populateTable(tickets);
        }
    }

    xhttp.open("GET", `http://localhost:9001/Project1/ticket/json/getUsersTickets`)

    xhttp.send();
}

function userInformation(userObject) {
    document.getElementById("welcomeText").innerText = "Welcome, " + userObject.username;
}

function populateTable(tickets) {
    
    // since this is the user page, we will only populate the users tickets

    
    for (let i=0; i<tickets.length; ++i) {
        // create the elements dynamically

        let newTr = document.createElement("tr");
        let newTh = document.createElement("th");
        let newTd1 = document.createElement("td");
        let newTd2 = document.createElement("td");
        let newTd3 = document.createElement("td");
        let newTd4 = document.createElement("td");
        let newTd5 = document.createElement("td");
        let newTd6 = document.createElement("td");
        let newTd7 = document.createElement("td");

        // create text values
        newTh.setAttribute("scope", "row");
        let thText = document.createTextNode(tickets[i].ticketId);
        let tdText1 = document.createTextNode(tickets[i].username);
        let tdText2 = document.createTextNode(`\$${tickets[i].ticketAmount}`);
        let tdText3 = document.createTextNode(tickets[i].ticketType);
        let tdText4 = document.createTextNode(tickets[i].ticketStatus.ticketStatus);
        let tdText5 = document.createTextNode(tickets[i].description);
        let tdText6 = document.createTextNode(tickets[i].time_submitted);
        let tdText7 = document.createTextNode(tickets[i].time_resolved);


        // append elements 
        newTh.appendChild(thText);
        newTd1.appendChild(tdText1);
        newTd2.appendChild(tdText2);
        newTd3.appendChild(tdText3);
        newTd4.appendChild(tdText4);
        newTd5.appendChild(tdText5);
        newTd6.appendChild(tdText6);
        newTd7.appendChild(tdText7);

        // throw elements in the row 
        newTr.appendChild(newTh);
        newTr.appendChild(newTd1);
        newTr.appendChild(newTd2);
        newTr.appendChild(newTd3);
        newTr.appendChild(newTd4);
        newTr.appendChild(newTd5);
        newTr.appendChild(newTd6);
        newTr.appendChild(newTd7);

        // append the row
        let newSelection = document.querySelector('#ticketTableBody');
        newSelection.appendChild(newTr);
    }
}

