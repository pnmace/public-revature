package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionGenerator {
	public static String dburl = "jdbc:postgresql://"+System.getenv("TRAINING_DB_ENDPOINT")+"/project1";
	public static String dbusername = System.getenv("TRAINING_DB_USERNAME");
	public static String dbpassword = System.getenv("TRAINING_DB_PASSWORD");
	
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(dburl, dbusername, dbpassword);
	}
}
