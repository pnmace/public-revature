package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jdbc.ConnectionGenerator;
import model.Ticket;
import model.TicketStatus;

public class TicketDaoImpl implements TicketDao {

	@Override
	public Ticket createTicket(int userId, int ticketAmount, String ticketType, String description) {
		Ticket ticket = null;
		try (Connection conn = ConnectionGenerator.getConnection()) {
			// get the username
			String request = "SELECT username FROM users WHERE user_id = ?";
			PreparedStatement ps = conn.prepareStatement(request);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			rs.next();
			String username = rs.getString(1);
			
		
			// create the ticket with the user_id, amount, timestamp
			Date date = new Date();
			Timestamp timestamp = new Timestamp(date.getTime());
			request = "INSERT INTO tickets (user_id, amount, description, time_submitted) values(?, ?, ?, ?)";
			ps = conn.prepareStatement(request);
			ps.setInt(1, userId);
			ps.setInt(2, ticketAmount);
			ps.setString(3, description);
			ps.setTimestamp(4, timestamp);
			ps.executeUpdate();
			
			// get the ticket id
			request = "SELECT max(ticket_id) FROM tickets WHERE user_id = ?";
			ps = conn.prepareStatement(request);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			rs.next();
			int ticketId = rs.getInt(1);
			
			// set the lookup table to have ticketid/type
			request = "INSERT INTO reimbursement_type values(?, ?)";
			ps = conn.prepareStatement(request);
			ps.setInt(1, ticketId);
			ps.setString(2, ticketType);
			ps.executeUpdate();
			
			// set the lookup table to have ticketid/status
			request = "INSERT INTO ticket_status values(?, 'pending')";
			ps = conn.prepareStatement(request);
			ps.setInt(1, ticketId);
			ps.executeUpdate();
			
			// create the TicketStatus object and new ticket
			TicketStatus status = new TicketStatus(ticketId, "pending");
			ticket = new Ticket(ticketId, userId, ticketAmount, ticketType, username, status, description, timestamp);
			
			System.out.println(ticket);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return ticket;
	}

	@Override
	public List<Ticket> getUsersTickets(int userId) {
		List<Ticket> ticketList = new ArrayList<Ticket>();
		
		try (Connection conn = ConnectionGenerator.getConnection()) {
		String request = "SELECT tickets.*, reimbursement_type.reimbursement_type,users.username, ticket_status.ticket_status\n"
				+ "FROM tickets\n"
				+ "LEFT JOIN reimbursement_type ON tickets.ticket_id = reimbursement_type.ticket_id\n"
				+ "LEFT JOIN ticket_status ON tickets.ticket_id = ticket_status.ticket_id\n"
				+ "LEFT JOIN users ON users.user_id = tickets.user_id\n"
				+ "WHERE tickets.user_id = ?";
		
		PreparedStatement ps = conn.prepareStatement(request);
		ps.setInt(1, userId);
		ResultSet rs = ps.executeQuery();
		
		//	1			2		3			4			5				6				7		8			9
		// ticket id, user id, amount, description, time_submitted, time_resolved, reimb_type, username, ticket_status
		while (rs.next()) {
			TicketStatus status = new TicketStatus(rs.getInt(1), rs.getString(9));
										//ticket	user_id			amount		    type			username	  status	description		time submitted		time resolved
			ticketList.add(new Ticket(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(7), rs.getString(8), status, rs.getString(4), rs.getTimestamp(5), rs.getTimestamp(6)));
		}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ticketList;
	}

	@Override
	public List<Ticket> getAllTickets() {
		List<Ticket> tickets = new ArrayList<Ticket>();
		try (Connection conn = ConnectionGenerator.getConnection()) {
		String request = "SELECT tickets.*, reimbursement_type.reimbursement_type, users.username, ticket_status.ticket_status\n"
				+ "FROM tickets\n"
				+ "FULL outer JOIN reimbursement_type ON tickets.ticket_id = reimbursement_type.ticket_id\n"
				+ "FULL outer JOIN ticket_status ON tickets.ticket_id = ticket_status.ticket_id\n"
				+ "INNER JOIN users ON users.user_id = tickets.user_id";

		PreparedStatement ps = conn.prepareStatement(request);
		ResultSet rs = ps.executeQuery();
		
		//	1			2		3			4			5				6				7		8			9
		// ticket id, user id, amount, description, time_submitted, time_resolved, reimb_type, username, ticket_status
		while (rs.next()) {
			TicketStatus status = new TicketStatus(rs.getInt(1), rs.getString(9));
										//ticket	user_id			amount		    type			username	  status	description		time submitted		time resolved
			tickets.add(new Ticket(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(7), rs.getString(8), status, rs.getString(4), rs.getTimestamp(5), rs.getTimestamp(6)));
		}
		
		
		
		// ticket id, user id, amount, type, username, status
//		while (rs.next()) {
//			TicketStatus status = new TicketStatus(rs.getInt(1), rs.getString(6));
//			tickets.add(new Ticket(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), status));
//		}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tickets;
	}

	@Override
	public Ticket getTicket(int ticketId) {
		Ticket ticket = null;
		try (Connection conn = ConnectionGenerator.getConnection()) {
		String request = "SELECT tickets.*, reimbursement_type.reimbursement_type, ticket_status.ticket_status\n"
				+ "FROM tickets\n"
				+ "FULL outer JOIN reimbursement_type ON tickets.ticket_id = reimbursement_type.ticket_id\n"
				+ "FULL outer JOIN ticket_status ON tickets.ticket_id = ticket_status.ticket_id\n"
				+ "WHERE tickets.ticket_id=?";
		
		PreparedStatement ps = conn.prepareStatement(request);
		ps.setInt(1, ticketId);
		ResultSet rs = ps.executeQuery();
		
		// ticket id, user id, amount, type, status
		if (rs.next()) {
			TicketStatus status = new TicketStatus(rs.getInt(1), rs.getString(5));
			ticket = new Ticket(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), status);
		}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ticket;
	}

	@Override
	public boolean setTicketStatus(TicketStatus ticketStatus) {
		try (Connection conn = ConnectionGenerator.getConnection()) {
			// this updates the ticket status in the lookup table
			String request = "\n"
					+ "UPDATE ticket_status\n"
					+ "SET ticket_status = ?\n"
					+ "WHERE ticket_id = ?";
			
			Date date = new Date();
			Timestamp timestamp = new Timestamp(date.getTime());
		
			PreparedStatement ps = conn.prepareStatement(request);
			ps.setString(1, ticketStatus.getTicketStatus());
			ps.setInt(2, ticketStatus.getTicketId());
			ps.executeUpdate();
			
			// this is setting the timestamp in the tickets table
			request = "UPDATE tickets\n"
					+ "SET time_resolved = ?\n"
					+ "WHERE ticket_id = ?";
			
			ps = conn.prepareStatement(request);
			ps.setTimestamp(1, timestamp);
			ps.setInt(2, ticketStatus.getTicketId());
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Override
	public boolean deleteTicket(int ticketId) {
		try (Connection conn = ConnectionGenerator.getConnection()) {
		String request = "DELETE FROM ticket_status WHERE ticket_id = ?;\n"
				+ "DELETE FROM reimbursement_type WHERE ticket_id = ?;\n"
				+ "DELETE FROM tickets WHERE ticket_id = ?";

		PreparedStatement ps = conn.prepareStatement(request);
		ps.setInt(1, ticketId);
		ps.setInt(2, ticketId);
		ps.setInt(3, ticketId);
		ps.executeUpdate();
		
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
