package dao;

import model.User;

public interface UserDao {
	// c
	public User createUser(String username, String password, String type);
	
	// r
	public User getUser(String username, String password);
	
	// u
	// d

}
