package dao;


import java.util.List;

import model.Ticket;
import model.TicketStatus;

public interface TicketDao {
	// c
	public Ticket createTicket(int userId, int ticketAmount, String ticketType, String description);
	
	// r
	public List<Ticket> getUsersTickets(int userId);
	public List<Ticket> getAllTickets();
	public Ticket getTicket(int ticketId);
	
	// u
	public boolean setTicketStatus(TicketStatus ticketStatus);
	
	// d
	public boolean deleteTicket(int ticketId);
}
