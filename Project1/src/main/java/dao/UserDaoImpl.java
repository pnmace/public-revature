package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jdbc.ConnectionGenerator;
import model.User;
import model.UserRole;

public class UserDaoImpl implements UserDao {

	@Override
	public User createUser(String username, String password, String type) {
		User user = null;
		int userId;
		
		try (Connection conn = ConnectionGenerator.getConnection()) {
			
			String request = "INSERT INTO users (username, password) values(?, ?)";
			PreparedStatement ps = conn.prepareStatement(request);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.executeUpdate();
			
			
			request = "SELECT user_id FROM users WHERE username = ?";
			ps = conn.prepareStatement(request);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next())
				userId = rs.getInt(1);
			else
				return null;
			
			
			request = "INSERT INTO account_role values(?, ?)";
			ps = conn.prepareStatement(request);
			ps.setInt(1, userId);
			ps.setString(2, type);
			ps.executeUpdate();
			

			UserRole accountType = new UserRole(userId, type);
			user = new User(userId, username, password, accountType);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	@Override
	public User getUser(String username, String password) {
		User user = null;
		try (Connection conn = ConnectionGenerator.getConnection()) {
			String request = "SELECT users.*, account_role.acctype\n"
					+ "FROM users\n"
					+ "INNER JOIN account_role ON users.user_id = account_role.user_id\n"
					+ "AND users.username = ? AND users.PASSWORD = ?;";
			PreparedStatement ps = conn.prepareStatement(request);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			// id, user, pw, type
			if (rs.next()) {
				int accId = rs.getInt(1);
				String type = rs.getString(4);
				UserRole accType = new UserRole(accId, type);
				user = new User(accId, username, password, accType);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

}
