package frontController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.HomeController;
import controller.LoginController;
import controller.TicketController;
import controller.UserController;

public class Dispatcher {

	public static void myRouter(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		System.out.println("----dispatcher----");
		System.out.println(req.getRequestURI());
		
		switch(req.getRequestURI()) {
		// login/logout
		case "/Project1/forwarding/login":
			LoginController.login(req, resp);
			break;
		case "/Project1/user/logout":
			LoginController.logout(req, resp);
			break;	
			
			
		// our homepage for employees
		case "/Project1/forwarding/home":
			HomeController.home(req, resp);
			break;
		// our admin page
		case "/Project1/forwarding/admin":
			HomeController.admin(req, resp);
			break;
			
			
		// handle user endpoints here
		case "/Project1/user/json/getCurrentUser":
			UserController.getCurrentUserFromSession(req, resp);
			break;
		case "/Project1/ticket/user/createUser":
			UserController.createUser(req, resp);
			break;
			
			
		// handle ticket endpoints here
		case "/Project1/ticket/json/createTicket":
			TicketController.createTicket(req, resp);
			break;

		case "/Project1/ticket/json/getUsersTickets":
			TicketController.getUsersTickets(req, resp);
			break;

		case "/Project1/ticket/json/getAllTickets":
			TicketController.getAllTickets(req, resp);
			break;

		case "/Project1/ticket/json/getTicket":
			TicketController.getTicketById(req, resp);
			break;
			
		case "/Project1/ticket/json/setTicketStatus":
			TicketController.setTicketStatus(req, resp);
			break;

		case "/Project1/ticket/json/deleteTicket":
			TicketController.deleteTicket(req, resp);
			break;

			
		default:
			System.out.println("bad uri");
			req.getRequestDispatcher("/resources/html/badcreds.html").forward(req, resp);
			break;
		}
	}
	
}
