package model;

public class UserRole {
	private int accountId;
	private String accountType;
	
	public UserRole() {
	}

	public UserRole(int accountId, String accountType) {
		this.accountId = accountId;
		this.accountType = accountType;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	@Override
	public String toString() {
		return "UserRole [accountId=" + accountId + ", accountType=" + accountType + "]";
	}
}
