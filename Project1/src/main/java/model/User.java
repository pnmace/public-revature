package model;

public class User {
	private int userId;
	private String username;
	private String password;
	private UserRole type;

	public User() {
	}

	/*
	 * this is likely the constructor I will be using, but I want to have
	 * the others here just in case
	 */
	public User(int userId, String username, String password, UserRole type) {
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.type = type;
	}

	public User(int userId, String username, String password) {
		this.userId = userId;
		this.username = username;
		this.password = password;
	}
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public UserRole getType() {
		return type;
	}

	public void setType(UserRole type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", password=" + password + ", type=" + type + "]";
	}
}
