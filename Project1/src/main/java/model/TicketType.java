package model;

public class TicketType {
	private int ticketId;
	private String ticketType;
	
	public TicketType() {
	}

	public TicketType(int ticketId, String ticketType) {
		this.ticketId = ticketId;
		this.ticketType = ticketType;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	@Override
	public String toString() {
		return "TicketType [ticketId=" + ticketId + ", ticketType=" + ticketType + "]";
	}
}
