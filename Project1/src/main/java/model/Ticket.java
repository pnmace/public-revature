package model;

import java.sql.Timestamp;

public class Ticket {
	private int ticketId;
	private int userId;
	private int ticketAmount;
	private String ticketType;
	private String username;
	private TicketStatus ticketStatus;
	private String description;
	private Timestamp time_submitted;
	private Timestamp time_resolved;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Ticket() {
	}
	
	public Ticket(int ticketId, int userId, int ticketAmount, String ticketType) {
		this.ticketId = ticketId;
		this.userId = userId;
		this.ticketAmount = ticketAmount;
		this.ticketType = ticketType;
	}

	public Ticket(int ticketId, int userId, int ticketAmount, String ticketType, TicketStatus ticketStatus) {
		this.ticketId = ticketId;
		this.userId = userId;
		this.ticketAmount = ticketAmount;
		this.ticketType = ticketType;
		this.ticketStatus = ticketStatus;
	}
	
	public Ticket(int ticketId, int userId, int ticketAmount, String ticketType, String username, TicketStatus ticketStatus) {
		this.ticketId = ticketId;
		this.userId = userId;
		this.ticketAmount = ticketAmount;
		this.ticketType = ticketType;
		this.username = username;
		this.ticketStatus = ticketStatus;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTicketAmount() {
		return ticketAmount;
	}

	public void setTicketAmount(int ticketAmount) {
		this.ticketAmount = ticketAmount;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	@Override
	public String toString() {
		return "Ticket [ticketId=" + ticketId + ", userId=" + userId + ", ticketAmount=" + ticketAmount
				+ ", ticketType=" + ticketType + ", username=" + username + ", ticketStatus=" + ticketStatus
				+ ", description=" + description + ", time_submitted=" + time_submitted + ", time_resolved="
				+ time_resolved + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getTime_submitted() {
		return time_submitted;
	}

	public void setTime_submitted(Timestamp time_submitted) {
		this.time_submitted = time_submitted;
	}

	public Timestamp getTime_resolved() {
		return time_resolved;
	}

	public void setTime_resolved(Timestamp time_resolved) {
		this.time_resolved = time_resolved;
	}

	public Ticket(int ticketId, int userId, int ticketAmount, String ticketType, String username,
			TicketStatus ticketStatus, String description, Timestamp time_submitted) {
		super();
		this.ticketId = ticketId;
		this.userId = userId;
		this.ticketAmount = ticketAmount;
		this.ticketType = ticketType;
		this.username = username;
		this.ticketStatus = ticketStatus;
		this.description = description;
		this.time_submitted = time_submitted;
	}

	public Ticket(int ticketId, int userId, int ticketAmount, String ticketType, String username,
			TicketStatus ticketStatus, String description, Timestamp time_submitted, Timestamp time_resolved) {
		super();
		this.ticketId = ticketId;
		this.userId = userId;
		this.ticketAmount = ticketAmount;
		this.ticketType = ticketType;
		this.username = username;
		this.ticketStatus = ticketStatus;
		this.description = description;
		this.time_submitted = time_submitted;
		this.time_resolved = time_resolved;
	}

	

	
}
