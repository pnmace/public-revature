package model;

public class TicketStatus {
	private int ticketId;
	private String ticketStatus;
	
	public TicketStatus() {
	}

	public TicketStatus(int ticketId, String ticketStatus) {
		this.ticketId = ticketId;
		this.ticketStatus = ticketStatus;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	@Override
	public String toString() {
		return "TicketStatus [ticketId=" + ticketId + ", ticketStatus=" + ticketStatus + "]";
	}
}
