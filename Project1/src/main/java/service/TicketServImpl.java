package service;

import java.util.List;

import dao.TicketDao;
import dao.TicketDaoImpl;
import model.Ticket;
import model.TicketStatus;

public class TicketServImpl implements TicketServ {

	private static TicketDao dao = new TicketDaoImpl();
	
	@Override
	public Ticket createTicket(int userId, int ticketAmount, String ticketType, String description) {

		return dao.createTicket(userId, ticketAmount, ticketType, description);
		
	}

	@Override
	public List<Ticket> getUsersTickets(int userId) {

		return dao.getUsersTickets(userId);
		
	}

	@Override
	public List<Ticket> getAllTickets() {

		return dao.getAllTickets();
		
	}

	@Override
	public Ticket getTicket(int ticketId) {

		return dao.getTicket(ticketId);
		
	}

	@Override
	public boolean setTicketStatus(TicketStatus ticketStatus) {

		return dao.setTicketStatus(ticketStatus);
		
	}

	@Override
	public boolean deleteTicket(int ticketId) {

		return dao.deleteTicket(ticketId);
		
	}

}
