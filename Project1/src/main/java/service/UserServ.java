package service;

import model.User;

public interface UserServ {
	// c
	public User createUser(String username, String password, String type);
	
	// r
	public User getUser(String username, String password);
	
	// u
	// d
}
