package service;

import dao.UserDao;
import dao.UserDaoImpl;
import model.User;

public class UserServImpl implements UserServ {
	private static UserDao dao = new UserDaoImpl();

	@Override
	public User createUser(String username, String password, String type) {

		return dao.createUser(username, password, type);
		
	}

	@Override
	public User getUser(String username, String password) {

		return dao.getUser(username, password);
		
	}

}
