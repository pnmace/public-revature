package testing;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import jdbc.ConnectionGenerator;

public class Main {

	public static void main(String[] args) {


		
		try (Connection conn = ConnectionGenerator.getConnection()) {
			
			
			
//			String request = "INSERT INTO food values(1, ?)";
//			PreparedStatement ps = conn.prepareStatement(request);
//			ps.setTimestamp(1, timestamp);
//			ps.executeUpdate();
			
			String request = "\n"
					+ "UPDATE ticket_status\n"
					+ "SET ticket_status = ?\n"
					+ "WHERE ticket_id = ?";
			
			Date date = new Date();
			Timestamp timestamp = new Timestamp(date.getTime());
		
			PreparedStatement ps = conn.prepareStatement(request);
			ps.setString(1, "approved");
			ps.setInt(2, 43);
			ps.executeUpdate();
			
			request = "UPDATE tickets\n"
					+ "SET time_resolved = ?\n"
					+ "WHERE ticket_id = ?";
			
			ps = conn.prepareStatement(request);
			ps.setTimestamp(1, timestamp);
			ps.setInt(2, 43);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
