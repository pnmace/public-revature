package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import jdbc.ConnectionGenerator;
import model.User;
import service.UserServ;
import service.UserServImpl;

public class LoginController {

	public static void login(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		UserServ serv = new UserServImpl();
		String loginPath = null;
		
		if (!req.getMethod().equals("POST")) {
			loginPath = "/login.html";
			req.getRequestDispatcher(loginPath).forward(req, resp);
			return;
		}

		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		try (Connection conn = ConnectionGenerator.getConnection()) {
			String request = "SELECT * FROM users WHERE username = ? AND PASSWORD = ?";
			
			PreparedStatement ps = conn.prepareStatement(request);
			ps.setString(1, username);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			
			// if we have a result, we know they entered their credentials correctly
			if (rs.next()) {
				// we will now get the correct user type
				User user = serv.getUser(username, password);
				String type = user.getType().getAccountType();
				int userId = user.getUserId();
				
				req.getSession().setAttribute("currentUsername", username);
				req.getSession().setAttribute("currentPassword", password);
				req.getSession().setAttribute("accountType", type);
				req.getSession().setAttribute("userId", userId);
				
				// so we send them to the respective page
				if (!type.equals("admin")) {
					loginPath = "/forwarding/home";
					req.getRequestDispatcher(loginPath).forward(req, resp);
				} else {
					loginPath = "/forwarding/admin";
					req.getRequestDispatcher(loginPath).forward(req, resp);
				}
				
			// otherwise we send them to an invalid page
			} else {
				loginPath = "/forwarding/invalidcase";
				req.getRequestDispatcher(loginPath).forward(req, resp);
				return;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		req.getSession().invalidate();

//		String redirectPath = "/login.html";
//		req.getRequestDispatcher(redirectPath).forward(req, resp);

		resp.getWriter().write(new ObjectMapper().writeValueAsString("ack"));
	}
	
	
}
