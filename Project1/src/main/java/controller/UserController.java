package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.User;
import service.UserServ;
import service.UserServImpl;

/* 
 * not really going to use this idt since I'd need async/await to make use of it
 */
public class UserController {

	public static void getCurrentUserFromSession(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		UserServ serv = new UserServImpl();
		String username = (String)req.getSession().getAttribute("currentUsername");
		String password = (String)req.getSession().getAttribute("currentPassword");
		
		User currentUser = serv.getUser(username, password);
		
		PrintWriter printer = resp.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(currentUser));
	}
	
	public static void createUser(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		UserServ serv = new UserServImpl();
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		String type = "employee";

		System.out.println(username);
		System.out.println(password);
		System.out.println(type);
		
		serv.createUser(username, password, type);

		
	}
	
	
}
