package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeController {
	public static void home(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		String homePath = "/resources/html/home.html";
		
		req.getRequestDispatcher(homePath).forward(req, resp);
	}
	
	public static void admin(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String adminPath = "/resources/html/admin.html";
		
		req.getRequestDispatcher(adminPath).forward(req, resp);
	}
}
