package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.Ticket;
import model.TicketStatus;
import service.TicketServ;
import service.TicketServImpl;

public class TicketController {
	
	// need to get a way to link user id with session id
	public static void createTicket(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		TicketServ serv = new TicketServImpl();
		
		int userId = (int)req.getSession().getAttribute("userId");
		int amount = Integer.parseInt(req.getParameter("amount"));
		String ticketType = req.getParameter("ticketType");
		String description = req.getParameter("description");
		
		serv.createTicket(userId, amount, ticketType,description);

		String returnPath = "/resources/html/home.html";
		req.getRequestDispatcher(returnPath).forward(req, res);
		
		
	}
	
	// need a way to get the user id field from a request
	public static void getUsersTickets(HttpServletRequest req, HttpServletResponse res) throws IOException {
		TicketServ serv = new TicketServImpl();
		
		int userId = (int) req.getSession().getAttribute("userId");
		
		List<Ticket> usersTickets = serv.getUsersTickets(userId);
		
		PrintWriter printer = res.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(usersTickets));
		
	}
	
	public static void getAllTickets(HttpServletRequest req, HttpServletResponse res) throws IOException {
		TicketServ serv = new TicketServImpl();
		List<Ticket> tickets = serv.getAllTickets();

		PrintWriter printer = res.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(tickets));
	}
	
	public static void getTicketById(HttpServletRequest req, HttpServletResponse res) throws IOException {
		TicketServ serv = new TicketServImpl();
		int ticketId = Integer.parseInt(req.getParameter("ticketId"));
		Ticket ticket = serv.getTicket(ticketId);

		PrintWriter printer = res.getWriter();
		printer.write(new ObjectMapper().writeValueAsString(ticket));
	}
	
	public static void setTicketStatus(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		TicketServ serv = new TicketServImpl();

		int ticketId = Integer.parseInt(req.getParameter("ticketId"));
		String ticketStatus = req.getParameter("ticketStatus");
		TicketStatus status = new TicketStatus(ticketId, ticketStatus);

		
		serv.setTicketStatus(status);
		
		// go back
		String returnPath = "/resources/html/admin.html";
		req.getRequestDispatcher(returnPath).forward(req, res);
	
	}
	
	public static void deleteTicket(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		TicketServ serv = new TicketServImpl();
		int ticketId = Integer.parseInt(req.getParameter("ticketId"));
		int userId = (int)req.getSession().getAttribute("userId");
		
		List<Ticket> userTickets = serv.getUsersTickets(userId);
		List<Integer> intlist = new ArrayList<Integer>();
		
		for (var i : userTickets) {
			intlist.add(i.getTicketId());
		}
		System.out.println(intlist);
		
		if (intlist.contains(ticketId)) {
			serv.deleteTicket(ticketId);
		} else {
			PrintWriter printer = res.getWriter();
			printer.write("This is not your ticket!");
			return;
		}

		// go back
		String returnPath = "/resources/html/home.html";
		req.getRequestDispatcher(returnPath).forward(req, res);
	}
	
}
